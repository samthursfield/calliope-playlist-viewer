# Calliope Playlist Viewer
# Copyright (C) 2019  Sam Thursfield <sam@afuera.me.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import flask
import pytest

import json
import logging
import sys

from cpv.backend import localcontent


"""Tests for the /local/ API provided by the backend server."""


# Test HTTP requests are created using the Werzkeug test API:
#
#   https://werkzeug.palletsprojects.com/en/0.15.x/test/

logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)


@pytest.fixture
def local_app():
    """Returns a Flask application with the /local endpoints available."""
    bp = localcontent.make_blueprint(playlist_text=None)
    app = flask.Flask(__name__)
    app.register_blueprint(bp)
    return app.test_client()


def test_ping(local_app):
    result = local_app.get('/local/')
    assert result.data == b'Hello world'


def test_playlists(local_app, shared_datadir):
    result = local_app.put('/local/_config/playlist_directories',
                           query_string={ 'list': json.dumps([str(shared_datadir)]) })
    assert result.status == '200 OK'
    assert result.json == { 'success': True }

    result = local_app.get('/local/playlists')
    assert result.json == [
        {'id': 'test-cd-1.xspf'}
    ]
