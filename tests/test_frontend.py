# Calliope Playlist Viewer
#
# Copyright 2019 Sam Thursfield <sam@afuera.me.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import pytest
import selenium.webdriver

import os


@pytest.fixture
def driver():
    # FIXME: we can improve this slightly if we depend on python3-selenium 3.14
    # or later; see:
    # https://blogs.igalia.com/carlosgc/2019/01/08/epiphany-automation-mode/
    options = selenium.webdriver.WebKitGTKOptions()
    options.binary_location = os.environ.get('CPV_BINARY', 'calliope-playlist-viewer')
    options.add_argument('--automation')
    options.add_argument('--debug')
    capabilities = options.to_capabilities()
    capabilities['browserName'] = 'Calliope Playlist Viewer'
    capabilities['version'] = '1'
    driver = selenium.webdriver.WebKitGTK(desired_capabilities=capabilities)
    try:
        yield driver
    finally:
        driver.quit()


def test_basic(driver):
    """A "smoke test" to prove that the app has opened to the correct view."""
    element = driver.find_element_by_css_selector('div.list-group')
