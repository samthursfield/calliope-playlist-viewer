# Developer notes

## The desktop app

Run the app with `--debug` to enable:

  * verbose logs written to stderr/stdout
  * the [WebKit inspector](https://trac.webkit.org/wiki/WebInspector) (use F12 or CTRL+SHIFT+C to open)
