# Calliope Playlist Viewer
# Copyright (C) 2019  Sam Thursfield <sam@afuera.me.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""A simple web server for use by the desktop version of the app.

This can also be used for local testing of the web version of the app.

"""

import flask

from gi.repository import Gio

import argparse
import collections
import json
import logging
import os
import sys

from . import localcontent

def argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('base_dir', type=str,
                        help="Directory where frontend files are stored.")
    parser.add_argument('--debug', action='store_true',
                        help="Write logs to stderr.")
    return parser


def cache_dir():
    path = os.path.join(GLib.get_user_cache_dir(), 'calliope-playlist-viewer')
    os.makedirs(path, exist_ok=True)
    return path


def server(base_dir, base_deps_dir, playlist_text):
    webapp = flask.Flask(__name__, static_folder='')

    @webapp.route('/')
    def root():
        return flask.send_from_directory(base_dir, 'index.html')

    @webapp.route('/playlist/<path:filename>')
    def playlist(filename):
        return flask.send_from_directory(base_dir, 'playlist.html')

    @webapp.route('/deps/<path:filename>')
    def dep_file(filename):
        return flask.send_from_directory(base_deps_dir, filename)

    @webapp.route('/<path:filename>')
    def file(filename):
        return flask.send_from_directory(base_dir, filename)

    @webapp.route('/desktop/open', methods=['POST'])
    def desktop_open():
        '''Open a local file using the default application.

        This is a helper method for use by the desktop app only. It's necessary
        because WebKit security restrictions prevent the JavaScript code from
        being able to do this.

        '''
        path = flask.request.args['path']
        Gio.AppInfo.launch_default_for_uri(path)
        # This request should only be called asynchronously, but we respond
        # with a redirect just in case.
        return flask.redirect(flask.url_for('root'))

    local_content_api = localcontent.make_blueprint(playlist_text)
    webapp.register_blueprint(local_content_api)

    return webapp


def main(base_dir, base_deps_dir, playlist_text=None):
    """Run web UI.

    The `playlist_text` given will parsed and displayed if shown. Otherwise, a
    list of all playlists found on the system will appear.

    """

    webapp = server(base_dir, base_deps_dir, playlist_text)
    webapp.run(host='0.0.0.0', port=5000)


if __name__ == '__main__':
    args = argument_parser().parse_args()
    if args.debug:
        logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    main(args.base_dir)
