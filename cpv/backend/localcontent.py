# Calliope Playlist Viewer
# Copyright (C) 2019  Sam Thursfield <sam@afuera.me.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""Local content API.

This module provides a web API to access local playlists. It's for use by
the desktop app. It's exposed as a Flask blueprint.

"""


from gi.repository import Gio

import calliope.import_
import flask

import json
import logging
import pathlib

log = logging.getLogger('backend.localcontent')


class Playlist():
    def __init__(self, filename, contents, id, title):
        self.filename = filename
        self.contents = contents
        self.id = id
        self.title = title


class PlaylistLoader():
    def __init__(self, scan=True):
        self.playlist_directories = []
        self.playlists = {}

        self.scan = scan
        self.need_rescan = scan

    def get_playlist_directories(self):
        return self.playlist_directories

    def set_playlist_directories(self, playlist_directories):
        self.playlist_directories = playlist_directories

        if self.scan:
            self.need_rescan = True

    def get_playlists(self):
        if self.need_rescan:
            self.rescan_playlists()

        return self.playlists

    def parse_and_add_playlist(self, path, text):
        # FIXME: What if the id was the hash of the playlist data?
        playlist_id = str(path)
        contents = calliope.import_.parse_xspf(text)
        if len(contents) == 0:
            log.debug("Empty playlist: %s", path)
        else:
            title = contents[0].get('playlist.title', path)
            self.playlists[playlist_id] = Playlist(path, contents, playlist_id, title)

    def rescan_playlists(self):
        self.playlists = {}

        def file_is_playlist(path):
            gfile = Gio.File.new_for_path(str(path))
            info = gfile.query_info(Gio.FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE, Gio.FileQueryInfoFlags.NONE)
            mime_type = info.get_attribute_string(Gio.FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE)
            if mime_type == 'application/xspf+xml':
                return True

        def scan_directory(path):
            log.info("Scanning %s", path)
            for item in path.iterdir():
                if item.is_file() and file_is_playlist(item):
                    log.debug("Got %s", item)

                    with open(item) as f:
                        self.parse_and_add_playlist(item.name, f.read())
                elif item.is_dir():
                    scan_directory(item)

        log.info("Scanning %i directories", len(self.playlist_directories))
        for path_name in self.playlist_directories:
            path = pathlib.Path(path_name)
            scan_directory(path)

        self.need_rescan = False


def make_blueprint(playlist_text):
    log.info("Creating /local/ blueprint")
    bp = flask.Blueprint('localcontent', __name__, url_prefix='/local')

    if playlist_text:
        loader = PlaylistLoader(scan=False)
        loader.parse_and_add_playlist('commandline', playlist_text)
    else:
        loader = PlaylistLoader(scan=True)

    @bp.route('/')
    def ping():
        return 'Hello world'

    @bp.route('/playlists')
    def get_playlists():
        result = []
        for p in sorted(loader.get_playlists().values(), key=lambda p: p.id):
            result.append({
                'id': p.id
            })
        return flask.jsonify(result)

    @bp.route('/playlist/<string:id>')
    def get_playlist_by_id(id):
        playlists = loader.get_playlists()
        try:
            return flask.jsonify({
                'id': id,
                'title': playlists[id].title,
                'contents': playlists[id].contents,
            })
        except KeyError:
            return flask.make_response('Not found', 404)

    @bp.route('/_config/playlist_directories', methods=['GET'])
    def get_playlist_directories():
        return flask.jsonify(loader.get_playlist_directories())

    @bp.route('/_config/playlist_directories', methods=['PUT'])
    def set_playlist_directories():
        try:
            logging.debug("PUT playlist_directories: %s", flask.request.args)
            directories = json.loads(flask.request.args['list'])
            log.debug("config: Got new directories list: %s", directories)
            loader.set_playlist_directories(directories)
            return flask.jsonify({ "success": True})
        except ValueError as e:
            logging.warning("PUT playlist_directories: %r", e)
            return flask.make_response(flask.jsonify({ "success": False}), 400)

    return bp
