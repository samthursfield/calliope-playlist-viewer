# Calliope Playlist Viewer
#
# Copyright 2019 Sam Thursfield <sam@afuera.me.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import gi
gi.require_version('Gtk', '3.0')
gi.require_version('WebKit2', '4.0')
from gi.repository import Gio, GLib, Gtk, WebKit2

import requests

import json
import logging
import sys
import urllib.parse


log = logging.getLogger('desktop.app')


def expand_xdg_directory_placeholder(dirname):
    if dirname == '&DESKTOP':
        return GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_DESKTOP)
    elif dirname == '&DOCUMENTS':
        return GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_DOCUMENTS)
    elif dirname == '&DOWNLOAD':
        return GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_DOWNLOAD)
    elif dirname == '&MUSIC':
        return GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_MUSIC)
    elif dirname == '&PUBLIC_SHARE':
        return GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_PUBLIC_SHARE)
    elif dirname == '&TEMPLATES':
        return GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_TEMPLATES)
    elif dirname == '&VIDEOS':
        return GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_VIDEOS)
    return dirname


class GtkApp(Gtk.Application):
    def __init__(self, base_uri, debug=False, automation=False, show_path='/'):
        self.application_id = 'com.github.ssssam.CalliopePlaylistViewer'

        self._window = None
        self._debug = debug
        self._automation = automation
        self._base_uri = base_uri
        self._show_path = show_path

        self._webview = None

        if self._automation:
            flags = Gio.ApplicationFlags.NON_UNIQUE
        else:
            flags = flags=Gio.ApplicationFlags.FLAGS_NONE

        Gtk.Application.__init__(self, application_id=self.application_id, flags=flags)

        self.settings = Gio.Settings(self.application_id)
        self.init_settings()

        self.webkit_context = WebKit2.WebContext.get_default()
        if self._automation:
            self.webkit_context.connect('automation-started', self.on_automation_started)
            self.webkit_context.set_automation_allowed(True)

        if self._debug:
            open_webkit_inspector_action = Gio.SimpleAction.new('open-webkit-inspector', None)
            open_webkit_inspector_action.connect('activate', self.do_open_webkit_inspector)
            self.add_action(open_webkit_inspector_action)
            self.set_accels_for_action('app.open-webkit-inspector', ['<Control><Shift>c', 'F12'])

    def init_settings(self):
        self.set_playlist_directories(self.settings.get_strv('playlist-directories'))
        self.settings.connect('changed::playlist-directories',
                              lambda *args: self.set_playlist_directories(self.settings.get_strv('playlist-directories')))

    def set_playlist_directories(self, dirs):
        dirs = filter(None, [expand_xdg_directory_placeholder(d) for d in dirs])
        url = urllib.parse.urljoin(self._base_uri, '/local/_config/playlist_directories')
        requests.put(url, params={'list': json.dumps(list(dirs))}).raise_for_status()

        logging.debug("Set playlist directories list to: %s", dirs)

        self.reload_webapp()

    def reload_webapp(self):
        # Make sure the app updates to show the new data.
        if self._webview is not None:
            self._webview.reload()

    def quit_cb(self, *args):
        self.quit()

    def do_activate(self):
        '''Show the main window.'''
        if not self._window:
            self._webview = self.create_webview()
            self._window = self.create_window(self._webview)

            uri = self._base_uri + self._show_path
            log.debug("Loading: %s", uri)
            self._webview.load_uri(uri)

        self._window.present()

    def create_webview(self):
        view = WebKit2.WebView(web_context=self.webkit_context,
                               is_controlled_by_automation=self._automation)
        view.show()

        view.connect('context-menu', self.do_context_menu)

        if self._automation:
            view.connect('close', self.quit_cb)

        settings = view.get_settings()

        if self._debug:
            settings.set_property('enable-developer-extras', True)
            inspector = view.get_inspector()
            inspector.show()

        return view

    def on_automation_started(self, context, session):
        # Called when automated tests are running.
        info = WebKit2.ApplicationInfo.new()
        info.set_name("Calliope Playlist Viewer")
        info.set_version(1, 0, 0)
        session.set_application_info(info)
        session.connect('create-web-view', lambda session: self._webview)

    def on_load_changed(self, webview, load_event):
        # Called when the webview loads a new page, for example because the
        # user clicked a link.
        if load_event == WebKit2.LoadEvent.FINISHED:
            self._back_button.set_sensitive(webview.can_go_back())
            self._forward_button.set_sensitive(webview.can_go_forward())

            self._headerbar.set_title(webview.get_title())

    def create_window(self, webview):
        window = Gtk.ApplicationWindow.new(self)
        window.set_size_request(1024, 728)
        window.set_title('Calliope Playlist Viewer')

        headerbar = Gtk.HeaderBar.new()
        headerbar.set_has_subtitle(False)
        headerbar.set_show_close_button(True)
        headerbar.set_title('Playlists')

        back_button = Gtk.Button.new_from_icon_name('go-previous-symbolic', Gtk.IconSize.BUTTON)
        forward_button = Gtk.Button.new_from_icon_name('go-next-symbolic', Gtk.IconSize.BUTTON)

        back_button.connect('clicked', lambda *args: webview.go_back())
        forward_button.connect('clicked', lambda *args: webview.go_forward())

        headerbar.pack_start(back_button)
        headerbar.pack_start(forward_button)

        headerbar.show_all()
        window.set_titlebar(headerbar)

        window.add(webview)

        self._headerbar = headerbar
        self._back_button = back_button
        self._forward_button = forward_button
        webview.connect('load-changed', self.on_load_changed)

        window.connect('destroy', self.quit_cb)

        return window

    def do_context_menu(self, view, menu, event, hit_test_result):
        if hit_test_result.context_is_selection():
            # Show menu with 'Copy' item only
            menu.remove_all()

            copy_item = WebKit2.ContextMenuItem.new_from_stock_action(WebKit2.ContextMenuAction.COPY)
            menu.append(copy_item)

            return False    # False means "show the menu"
        else:
            return True     # True means "don't show the menu"

    def do_open_webkit_inspector(self, action, params):
        log.info("Opening WebKit inspector")
        self._webview.get_inspector().show()

    def run(self):
        '''Run the app until quit(), then raise any unhandled exception.'''
        self._exception = None

        old_hook = sys.excepthook

        def new_hook(etype, evalue, etb):
            log.error("Caught exception inside GTK main loop: %s %s %s", etype, evalue, etb)
            self.quit()
            self._exception = evalue

        try:
            sys.excepthook = new_hook
            Gtk.Application.run(self)
        finally:
            sys.excepthook = old_hook

        if self._exception:
            raise self._exception
